#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Point.hpp"
#include "Ray.hpp"

// Camera information
const double DEFAULT_ASPECT_RATIO = 16.0 / 9.0;
const double DEFAULT_VIEWPORT_HEIGHT = 2.0;
const double DEFAULT_VIEWPORT_WIDTH = DEFAULT_ASPECT_RATIO * DEFAULT_VIEWPORT_HEIGHT;
const double DEFAULT_FOCAL_LENGTH = 1.0;

// TODO: add functionality to read defaults from JSON file

class Camera
{
public:
   Camera();
   ~Camera() = default;

   Point GetOrigin() const;
   Point GetHorizontal() const;
   Point GetVertical() const;
   Point GetLowerLeftCorner() const;

   Ray GetRay(double u, double v) const;

private:
   double aspect_ratio_;
   double viewport_height_;
   double viewport_width_;
   double focal_length_;
   Point origin_;
   Point horizontal_;
   Point vertical_;
   Point lower_left_corner_;
};

#endif // CAMERA_HPP