#ifndef COLOR_HPP
#define COLOR_HPP

#include "Color.hpp"

#include <iostream>

class Color
{
public:
   Color();
   Color(double r, double g, double b);
   Color(double r, double g, double b, double a);
   double operator[](int i) const;
   double &operator[](int i);
   Color &operator+=(const Color &rhs);
   Color &operator-=(const Color &rhs);
   Color &operator*=(const double rhs);
   Color &operator/=(const double rhs);

   double GetR() const;
   double GetG() const;
   double GetB() const;
   double GetA() const;

   // write the format needed
   void WritePixelInt(std::ostream &out);
   void WritePixelInt(std::ostream &out, int multisample);

private:
   double r_;
   double g_;
   double b_;
   double a_;
};

// Utility Functions
std::ostream &operator<<(std::ostream &out, const Color &c);
Color operator+(const Color &lhs, const Color &rhs);
Color operator-(const Color &lhs, const Color &rhs);
Color operator*(const Color &lhs, const Color &rhs);
Color operator*(double lhs, const Color &rhs);
Color operator*(const Color &lhs, double rhs);
Color operator/(Color lhs, double rhs);

#endif // COLOR_HPP