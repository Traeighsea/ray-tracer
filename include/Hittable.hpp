#ifndef HITTABLE_HPP
#define HITTABLE_HPP

#include <optional>
#include "Point.hpp"

class Ray;

struct Collision
{
   Point p;
   Point normal;
   double t;

   bool front_face;

   void SetFaceNormal(const Ray &ray, const Point &outward_normal);
};

class Hittable
{
public:
   virtual std::optional<Collision> Hit(const Ray &r, double t_min, double t_max) const = 0;
};

#endif // HITTABLE_HPP