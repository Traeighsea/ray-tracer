#ifndef POINT_HPP
#define POINT_HPP

#include <cmath>
#include <iostream>
#include "../include/Util.hpp"

class Point
{
public:
   Point();
   Point(double x, double y, double z);
   Point operator-() const;
   double operator[](int i) const;
   double &operator[](int i);
   Point &operator+=(const Point &rhs);
   Point &operator*=(const double rhs);
   Point &operator/=(const double rhs);

   double GetX() const;
   double GetY() const;
   double GetZ() const;

   double Length() const;
   double LengthSquared() const;
   double Dot(const Point &v);
   Point Cross(const Point &v);
   Point UnitVector();

private:
   double x_;
   double y_;
   double z_;
};

// Utility Functions
std::ostream &operator<<(std::ostream &out, const Point &v);
Point operator+(const Point &lhs, const Point &rhs);
Point operator-(const Point &lhs, const Point &rhs);
Point operator*(const Point &lhs, const Point &rhs);
Point operator*(double lhs, const Point &rhs);
Point operator*(const Point &lhs, double rhs);
Point operator/(Point lhs, double rhs);

namespace util
{
   inline Point RandomPoint()
   {
      return Point(util::RandomDouble(-1.0, 1.0), util::RandomDouble(-1.0, 1.0), util::RandomDouble(-1.0, 1.0));
   }
   inline Point RandomPoint(double min, double max)
   {
      return Point(util::RandomDouble(min, max), util::RandomDouble(min, max), util::RandomDouble(min, max));
   }
   /// Returns a random point within the unit sphere
   inline Point RandInUnitSphere()
   {
      Point p(2, 2, 2);
      while (p.LengthSquared() > 1)
         p = RandomPoint();
      return p;
   }
   inline Point RandUnitVector()
   {
      return RandInUnitSphere().UnitVector();
   }
   inline Point RandInHemisphere(const Point &normal)
   {
      Point in_unit_sphere = RandInUnitSphere();
      if (in_unit_sphere.Dot(normal) > 0.0) // In the same hemisphere as the normal
         return in_unit_sphere;
      else
         return -in_unit_sphere;
   }
} // namespace util

#endif // POINT_HPP