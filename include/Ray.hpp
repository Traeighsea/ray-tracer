#ifndef RAY_HPP
#define RAY_HPP

#include "Point.hpp"
#include "Color.hpp"
#include "Hittable.hpp"

class Ray
{
public:
   Ray();
   Ray(const Point &origin, const Point &direction);

   Point At(double t) const;

   Point GetOrigin() const;
   Point GetDirection() const;

   Color Cast(const Color &start_color, const Color &end_color);
   Color Cast(const Color &start_color, const Color &end_color, const std::vector<std::shared_ptr<Hittable>> &objects, int depth);
   Color LinearBlend(const Color &start_color, const Color &end_color);

   std::optional<Collision> Hit(double t_min, double t_max, const std::vector<std::shared_ptr<Hittable>> &objects) const;

private:
   Point origin_;
   Point direction_;
};

#endif // RAY_HPP