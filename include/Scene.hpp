#ifndef SCENE_HPP
#define SCENE_HPP

#include "Hittable.hpp"
#include "Camera.hpp"

#include <memory>
#include <vector>

// Image parameters
const int IMAGE_WIDTH = 400;
const int IMAGE_HEIGHT = static_cast<int>(IMAGE_WIDTH / DEFAULT_ASPECT_RATIO);
const char *const COLOR_TYPE = "P3";
const int MAX_COLOR = 256;
const int SAMPLES_PER_PIXEL = 100;
const int RAY_BOUNCE_LIMIT = 50;

class Scene
{
public:
   Scene();
   Scene(std::shared_ptr<Hittable> object);

   void Clear();
   void Add(std::shared_ptr<Hittable> object);

   void Render();

public:
   std::vector<std::shared_ptr<Hittable>> objects_;
   // Scene information
   Camera camera_;
};

#endif // SCENE_HPP