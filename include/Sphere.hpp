#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "Hittable.hpp"
#include "Point.hpp"

class Sphere : public Hittable
{
public:
   Sphere();
   Sphere(Point cen, double r);

   virtual std::optional<Collision> Hit(
       const Ray &ray, double tmin, double tmax) const override;

public:
   Point center_;
   double radius_;
};

#endif // SPHERE_HPP