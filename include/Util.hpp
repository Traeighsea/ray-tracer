#ifndef UTIL_HPP
#define UTIL_HPP

#include <cmath>
#include <limits>
#include <memory>
#include <random>

namespace util
{

   // Constants

   const double infinity = std::numeric_limits<double>::infinity();
   const double pi = 3.1415926535897932385;

   // Utility Functions

   inline double DegreesToRadians(double degrees)
   {
      return degrees * pi / 180.0;
   }

   /// Returns a random number between 0 and 1.0
   /// Note: We are creating these functions, as opposed to always using the
   ///   std lib to allow us to have multiple different versions/variations of
   ///   randomness implemented later
   inline double RandomDouble()
   {
      static std::uniform_real_distribution<double> distribution(0.0, 1.0);
      static std::mt19937 generator;
      return distribution(generator);
   }

   inline double RandomDouble(double min, double max)
   {
      static std::uniform_real_distribution<double> distribution(min, max);
      static std::mt19937 generator;
      return distribution(generator);
   }

   inline double Clamp(double x, double min, double max)
   {
      if (x < min)
         return min;
      if (x > max)
         return max;
      return x;
   }

} // namespace util

#endif // UTIL_HPP