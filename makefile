#Specify which compiler we're using
CC = clang++
GPP = g++

#COMPILER_FLAGS specifies additional compilation options
COMPILER_FLAGS = -std=c++17 

#INCLUDE_FLAGS specifies the additonal include directories
INCLUDE_FLAGS = 

#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS =

#OBJS specifies which files to compile as part of the project
OBJS = src/Color.cpp src/Point.cpp src/Ray.cpp src/Sphere.cpp src/Hittable.cpp src/Camera.cpp src/Scene.cpp

#Specify the main function you want:
MAIN = Main.cpp
TEST = MainTest.cpp

#Output file name
OBJ_NAME = app

#This is the general format that compiles our executable:
make: $(OBJS)
	$(CC) $(COMPILER_FLAGS) $(INCLUDE_FLAGS) $(LINKER_FLAGS) $(OBJS) src/$(MAIN) -o bin/$(OBJ_NAME)
test: $(OBJS)
	$(CC) $(COMPILER_FLAGS) $(INCLUDE_FLAGS) $(LINKER_FLAGS) $(OBJS) src/$(TEST) -o bin/$(OBJ_NAME)
clean: 
	rm -f bin/*.o bin/image.ppm
run:
	bin/./app > bin/image.ppm