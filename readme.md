# Ray Tracer project

## Introduction

Back in 2015 I made a Ray Tracer using C, and then converted it into C++ for one of my computer science classes at Clemson. It was beautiful, it output ppm images from a scene that had a collection of various objects, materials, and lighting. It was a marvel to behold with complex vector calculations, matrix computations that would leave mathematicians bewildered, and beautiful images that would make a grown man cry. This is that very same masterfully crafted marvel to behold that could only be described as a gesamtkunstwerk in it's entirety, the one, the only PROJEEE-entirely kidding, unfortunately that particular project was done by ssh-ing into the Clemson computers, and having been created entirely on those computers, and I having been the pinnacle of forethought, it thus has been lost to time. I wish I had pulled those files before graduating, but alas I did not. Those files are now in the dark endless abyss never to be seen again by mortal eyes. So you'll just have to believe me on the caliber of greatness from which this code was masterfully crafted. It is such unfortunate circumstance...BUT WATCH, imma do it again.

## The Plan

So the plan is to start from scratch, probably for the best to be entirely honest, and this time eventually add OpenGL to the mix. It will have much better design, more performant algorithms, and...*checks notes*...extensive documentation. One thing that I know was missing is more advanced processing of images and complex shaders. Additionally there were no real time scenes, it would only output a ppm image. This should be much more achievable using OpenGL as opposed to just calculating pixel colors and outputting to a ppm image. Additionally I would like to keep the functionality to output images from the scene, but who even uses ppm images? Not me. So I intend to use a library to output png images instead. Overall this should be a great project to look more at the lower level implementation of graphics libraries, sorry SDL2 I will not be using you for the 100th time, and help to build my performance optimization knowledge. There's also the possibility later to add Vulkan support once I feel I have gone as far as I can with OpenGL. Graphics programming is very interesting to me so this will be a complete journey of that.

## Feature Roadmap

1. Scene Objects
2. Shading
3. Antialiasing
4. Diffusion
5. Materials
6. Reflection
7. Refraction
8. Dielectrics
9. Camera
10. Defocus Blur
11. Motion Blur
12. Bounding Volume Hierarchies
13. Solid Textures
14. Perlin Noise
15. Image Texture Mapping
16. Lights
17. Instance
18. Volumes
19. Triangles
20. Surface Textures
21. BUT WAIT, there's more...

## Technologies

Technologies I intend to use throughout this project:

* C++ 17
* OpenGl
* libpng
* Boost Libraries
* Boost Test Library
* GNU Make
* Git
* Clang Format
* Doxygen

## Running

You should be able to compile with make and then run with the following command:

```bash
make

make run
```

## Citations and Resources Used

* [_Ray Tracing in One Weekend_](https://raytracing.github.io/books/RayTracingInOneWeekend.html)
