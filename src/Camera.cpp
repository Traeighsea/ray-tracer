#include "../include/Camera.hpp"

Camera::Camera() : aspect_ratio_(DEFAULT_ASPECT_RATIO),
                   viewport_height_(DEFAULT_VIEWPORT_HEIGHT),
                   viewport_width_(DEFAULT_VIEWPORT_WIDTH),
                   focal_length_(DEFAULT_FOCAL_LENGTH),
                   origin_(0, 0, 0),
                   horizontal_(DEFAULT_VIEWPORT_WIDTH, 0.0, 0.0),
                   vertical_(0.0, DEFAULT_VIEWPORT_HEIGHT, 0.0),
                   lower_left_corner_(origin_ - horizontal_ / 2 - vertical_ / 2 - Point(0, 0, focal_length_))
{
}

Point Camera::GetOrigin() const { return origin_; }

Point Camera::GetHorizontal() const { return horizontal_; }

Point Camera::GetVertical() const { return vertical_; }

Point Camera::GetLowerLeftCorner() const { return lower_left_corner_; }

Ray Camera::GetRay(double u, double v) const
{
   return Ray(origin_, lower_left_corner_ + u * horizontal_ + v * vertical_ - origin_);
}