#include "../include/Color.hpp"
#include "../include/Util.hpp"

Color::Color() : r_(0), g_(0), b_(0), a_(0) {}

Color::Color(double r, double g, double b) : r_(r), g_(g), b_(b), a_(0) {}

Color::Color(double r, double g, double b, double a) : r_(r), g_(g), b_(b), a_(a) {}

double Color::operator[](int i) const
{
   if (i == 0)
   {
      return r_;
   }
   else if (i == 1)
   {
      return g_;
   }
   else if (i == 2)
   {
      return b_;
   }
   else if (i == 3)
   {
      return a_;
   }
   else
   {
      return r_;
   }
}

double &Color::operator[](int i)
{
   if (i == 0)
   {
      return r_;
   }
   else if (i == 1)
   {
      return g_;
   }
   else if (i == 2)
   {
      return b_;
   }
   else if (i == 3)
   {
      return a_;
   }
   else
   {
      return r_;
   }
}

Color &Color::operator+=(const Color &rhs)
{
   r_ += rhs[0];
   g_ += rhs[1];
   b_ += rhs[2];
   a_ += rhs[3];
   return *this;
}

Color &Color::operator-=(const Color &rhs)
{
   r_ -= rhs[0];
   g_ -= rhs[1];
   b_ -= rhs[2];
   a_ += rhs[3];
   return *this;
}

Color &Color::operator*=(const double rhs)
{
   r_ *= rhs;
   g_ *= rhs;
   b_ *= rhs;
   a_ *= rhs;
   return *this;
}

Color &Color::operator/=(const double rhs)
{
   return *this *= 1 / rhs;
}

double Color::GetR() const { return r_; }

double Color::GetG() const { return g_; }

double Color::GetB() const { return b_; }

double Color::GetA() const { return a_; }

void Color::WritePixelInt(std::ostream &out)
{
   // Write the translated [0,255] value of each color component.
   out << static_cast<int>(255.999 * r_) << ' '
       << static_cast<int>(255.999 * g_) << ' '
       << static_cast<int>(255.999 * b_) << '\n';
}

void Color::WritePixelInt(std::ostream &out, int multisample)
{
   const double gamma = 2.0;
   // Divide the color by the number of samples.
   double scale = 1.0 / multisample;

   r_ = std::pow(r_ * scale, 1.0 / gamma);
   g_ = std::pow(g_ * scale, 1.0 / gamma);
   b_ = std::pow(b_ * scale, 1.0 / gamma);

   // Write the translated [0,255] value of each color component.
   out << static_cast<int>(256 * util::Clamp(r_, 0.0, 0.999)) << ' '
       << static_cast<int>(256 * util::Clamp(g_, 0.0, 0.999)) << ' '
       << static_cast<int>(256 * util::Clamp(b_, 0.0, 0.999)) << '\n';
}

// Utility Functions
std::ostream &operator<<(std::ostream &out, const Color &c)
{
   return out << c[0] << ' ' << c[1] << ' ' << c[2];
}

Color operator+(const Color &lhs, const Color &rhs)
{
   return Color(lhs[0] + rhs[0], lhs[1] + rhs[1], lhs[2] + rhs[2]);
}

Color operator-(const Color &lhs, const Color &rhs)
{
   return Color(lhs[0] - rhs[0], lhs[1] - rhs[1], lhs[2] - rhs[2]);
}

Color operator*(const Color &lhs, const Color &rhs)
{
   return Color(lhs[0] * rhs[0], lhs[1] * rhs[1], lhs[2] * rhs[2]);
}

Color operator*(double lhs, const Color &rhs)
{
   return Color(lhs * rhs[0], lhs * rhs[1], lhs * rhs[2]);
}

Color operator*(const Color &lhs, double rhs)
{
   return rhs * lhs;
}

Color operator/(Color lhs, double rhs)
{
   return (1 / rhs) * lhs;
}