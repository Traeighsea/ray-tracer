#include "../include/Hittable.hpp"
#include "../include/Ray.hpp"

void Collision::SetFaceNormal(const Ray &ray, const Point &outward_normal)
{
   front_face = (ray.GetDirection()).Dot(outward_normal) < 0;
   normal = front_face ? outward_normal : -outward_normal;
}