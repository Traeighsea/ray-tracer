#include "../include/Scene.hpp"
#include "../include/Sphere.hpp"

int main()
{
   // Create Scene
   Scene scene;
   scene.Add(std::make_shared<Sphere>(Point(0, 0, -1), 0.5));
   scene.Add(std::make_shared<Sphere>(Point(0, -100.5, -1), 100));

   // TODO: make this real time by adding a loop and exit condition
   scene.Render();
}