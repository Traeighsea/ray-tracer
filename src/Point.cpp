#include "../include/Point.hpp"

Point::Point() : x_(0), y_(0), z_(0) {}

Point::Point(double x, double y, double z) : x_(x), y_(y), z_(z) {}

Point Point::operator-() const { return Point(-x_, -y_, -z_); }

double Point::operator[](int i) const
{
   if (i == 0)
      return x_;
   else if (i == 1)
      return y_;
   else if (i == 2)
      return z_;
   else
      return x_;
}

double &Point::operator[](int i)
{
   if (i == 0)
      return x_;
   else if (i == 1)
      return y_;
   else if (i == 2)
      return z_;
   else
      return x_;
}

Point &Point::operator+=(const Point &rhs)
{
   x_ += rhs[0];
   y_ += rhs[1];
   z_ += rhs[2];
   return *this;
}

Point &Point::operator*=(const double rhs)
{
   x_ *= rhs;
   y_ *= rhs;
   z_ *= rhs;
   return *this;
}

Point &Point::operator/=(const double rhs)
{
   return *this *= 1 / rhs;
}

double Point::GetX() const { return x_; }

double Point::GetY() const { return y_; }

double Point::GetZ() const { return z_; }

double Point::Length() const
{
   return std::sqrt(LengthSquared());
}

double Point::LengthSquared() const
{
   return x_ * x_ + y_ * y_ + z_ * z_;
}

double Point::Dot(const Point &v)
{
   return x_ * v[0] + y_ * v[1] + z_ * v[2];
}

Point Point::Cross(const Point &v)
{
   return Point(y_ * v[2] - z_ * v[1],
                z_ * v[0] - x_ * v[2],
                x_ * v[1] - y_ * v[0]);
}

Point Point::UnitVector()
{
   return (*this) / (*this).Length();
}

// Utility Functions
std::ostream &operator<<(std::ostream &out, const Point &v)
{
   return out << v[0] << ' ' << v[1] << ' ' << v[2];
}

Point operator+(const Point &lhs, const Point &rhs)
{
   return Point(lhs[0] + rhs[0], lhs[1] + rhs[1], lhs[2] + rhs[2]);
}

Point operator-(const Point &lhs, const Point &rhs)
{
   return Point(lhs[0] - rhs[0], lhs[1] - rhs[1], lhs[2] - rhs[2]);
}

Point operator*(const Point &lhs, const Point &rhs)
{
   return Point(lhs[0] * rhs[0], lhs[1] * rhs[1], lhs[2] * rhs[2]);
}

Point operator*(double lhs, const Point &rhs)
{
   return Point(lhs * rhs[0], lhs * rhs[1], lhs * rhs[2]);
}

Point operator*(const Point &lhs, double rhs)
{
   return rhs * lhs;
}

Point operator/(Point lhs, double rhs)
{
   return (1 / rhs) * lhs;
}