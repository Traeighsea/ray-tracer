#include "../include/Ray.hpp"
#include "../include/Hittable.hpp"

Ray::Ray() {}

Ray::Ray(const Point &origin, const Point &direction)
    : origin_(origin), direction_(direction)
{
}

Point Ray::GetOrigin() const
{
   return origin_;
}

Point Ray::GetDirection() const
{
   return direction_;
}

Point Ray::At(double t) const
{
   return origin_ + t * direction_;
}

Color Ray::Cast(const Color &start_color, const Color &end_color)
{
   return LinearBlend(start_color, end_color);
}

Color Ray::Cast(const Color &start_color, const Color &end_color, const std::vector<std::shared_ptr<Hittable>> &objects, int depth)
{
   // If we've exceeded the ray bounce limit, no more light is gathered.
   if (depth <= 0)
      return Color(0, 0, 0);

   // Check collisions and cast the ray
   std::optional<Collision> collision = Hit(0.001, util::infinity, objects);
   if (collision.has_value())
   {
      static bool lambertian_diffusion = true;
      if (lambertian_diffusion)
      {
         Point target = Point(collision.value().p + collision.value().normal + util::RandUnitVector());
         Ray ray_bounce = Ray(collision.value().p, target - collision.value().p);
         return 0.5 * ray_bounce.Cast(start_color, end_color, objects, depth - 1);
      }
      else
      {
         Point target = collision.value().p + util::RandInHemisphere(collision.value().normal);
         Ray ray_bounce = Ray(collision.value().p, target - collision.value().p);
         return 0.5 * ray_bounce.Cast(start_color, end_color, objects, depth - 1);
      }
   }
   else
   {
      return Cast(start_color, end_color);
   }
}

Color Ray::LinearBlend(const Color &start_color, const Color &end_color)
{
   Point unit_direction = GetDirection().UnitVector();
   double transition = 0.5 * (unit_direction.GetY() + 1.0);
   return (1.0 - transition) * start_color + transition * end_color;
}

std::optional<Collision> Ray::Hit(double t_min, double t_max, const std::vector<std::shared_ptr<Hittable>> &objects) const
{
   std::optional<Collision> collision = std::nullopt;
   std::optional<Collision> col_it = std::nullopt; // Used to iterate collisions
   double closest = t_max;

   for (const auto &object : objects)
   {
      col_it = object->Hit(*this, t_min, closest);
      if (col_it.has_value())
      {
         closest = col_it.value().t;
         collision = col_it;
      }
   }

   return collision;
}