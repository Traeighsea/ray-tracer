#include "../include/Scene.hpp"
#include "../include/Util.hpp"
#include "../include/Ray.hpp"

Scene::Scene() : objects_(),
                 camera_()
{
}
Scene::Scene(std::shared_ptr<Hittable> object) : objects_(),
                                                 camera_()
{
   Add(object);
}

void Scene::Clear() { objects_.clear(); }

void Scene::Add(std::shared_ptr<Hittable> object) { objects_.push_back(object); }

void Scene::Render()
{
   // Output header data
   std::cout << COLOR_TYPE << std::endl;
   std::cout << IMAGE_WIDTH << ' ' << IMAGE_HEIGHT << std::endl;
   std::cout << MAX_COLOR << std::endl;

   // Output image data
   std::cerr << "\nCreating image...\n";
   for (int j = IMAGE_HEIGHT - 1; j >= 0; j--)
   {
      std::cerr << "\r  Rows remaining: " << j << ' ' << std::flush;
      for (int i = 0; i < IMAGE_WIDTH; i++)
      {
         // temp start and end values
         const Color start(1.0, 1.0, 1.0);
         const Color end(0.5, 0.7, 1.0);

         Color pixel(0, 0, 0);
         for (int s = 0; s < SAMPLES_PER_PIXEL; s++)
         {
            double u = (i + util::RandomDouble()) / (IMAGE_WIDTH - 1);
            double v = (j + util::RandomDouble()) / (IMAGE_HEIGHT - 1);
            Ray ray = camera_.GetRay(u, v);
            pixel += ray.Cast(start, end, objects_, RAY_BOUNCE_LIMIT);
         }

         // Write pixel to the image
         pixel.WritePixelInt(std::cout, SAMPLES_PER_PIXEL);
      }
   }
   std::cerr << "\n...finished.\n";
}