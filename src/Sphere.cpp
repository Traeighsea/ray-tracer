#include "../include/Sphere.hpp"
#include "../include/Ray.hpp"

Sphere::Sphere() {}
Sphere::Sphere(Point cen, double r) : center_(cen), radius_(r){};

std::optional<Collision> Sphere::Hit(const Ray &ray, double t_min, double t_max) const
{
   std::optional<Collision> collision = std::nullopt;
   Point distance_from_center = ray.GetOrigin() - center_;

   // We're essentially finding the sides of a triangle, and then using the
   //   quadratic formula to determine the discriminate. This was further
   //   simplified as follows:
   double base = (ray.GetDirection()).LengthSquared();
   double leg = distance_from_center.Dot(ray.GetDirection());
   double hypotenuse = distance_from_center.LengthSquared() - radius_ * radius_;
   double discriminant = leg * leg - base * hypotenuse;

   if (discriminant > 0)
   {
      double root = sqrt(discriminant);
      double temp = (-leg - root) / base;
      if (temp < t_max && temp > t_min)
      {
         Point p = ray.At(temp);
         collision = Collision{
             p,
             Point(0, 0, 0),
             temp,
             false};
         Point outward_normal = (p - center_) / radius_;
         collision.value().SetFaceNormal(ray, outward_normal);
         return collision;
      }

      temp = (-leg + root) / base;
      if (temp < t_max && temp > t_min)
      {
         Point p = ray.At(temp);
         collision = Collision{
             p,
             Point(0, 0, 0),
             temp,
             false};
         Point outward_normal = (p - center_) / radius_;
         collision.value().SetFaceNormal(ray, outward_normal);
         return collision;
      }
   }

   return collision;
}